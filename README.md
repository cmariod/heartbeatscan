# README #

This README, documents whatever steps are necessary to get this project up and running.

### About ###

This prototype scans for user heart beat rate using smartphone camera and flashlight.

Depends on CorePlot and GPUImage

### How to set up? ###

* Create a container directory, named it whatever you like
* Go inside this container directory, clone this project
* on the same container directory, clone [GPUImage](https://github.com/BradLarson/GPUImage)
* on the same container directory, clone [CorePlot](https://github.com/core-plot/core-plot)

### Compatibility ###

iOS 9 using GPUImage 0.1.7 and CorePlot 2.0

### Roadmap ###

Put this under cocoapods and manage the dependencies using cocoapods as well 

### Who do I talk to? ###

* repo owner: cmariod@gmail.com