//
//  ScanViewController.h
//  HeartBeatDetect
//
//  Created by Mario Dinata on 11/11/15.
//  Copyright © 2015 Tribal Worldwide SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "CorePlot-CocoaTouch.h"
#import "PlotItem.h"

#define movingAverageArraySize      4
#define stabilizeDuration           8

@interface ScanViewController : UIViewController <GPUImageVideoCameraDelegate> {
    GPUImageVideoCamera *videoCamera;
    GPUImageOutput<GPUImageInput> *filter;
    
    PlotItem *detailItem;
    
    BOOL isStarted;
    NSDate *start;
    NSUInteger bpm;
    
    NSUInteger chartX;
    NSUInteger timerX;
    
    double movingAverageArray[movingAverageArraySize];
    double previousMovingAverage;
    double previousRedness;
    BOOL isDetectingPeak;
    NSUInteger peakCount;
    double maxRedness;
    
    NSMutableArray *HBRData;
    
    NSTimer *currentTimeTimer;
}

@property (strong, nonatomic) IBOutlet UIView *chartView;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *startStopButton;
@property (weak, nonatomic) IBOutlet UILabel *BPMLabel;

@end
