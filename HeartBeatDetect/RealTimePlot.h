//
//  RealTimePlot.h
//  CorePlotGallery
//

#import "PlotItem.h"

@interface RealTimePlot : PlotItem<CPTPlotDataSource>

-(void)newData:(NSTimer *)theTimer;
-(void)setData:(NSArray *)yArray;

@end
