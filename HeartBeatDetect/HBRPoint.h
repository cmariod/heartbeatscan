//
//  HBRPoint.h
//  af
//
//  Created by Mario Dinata on 11/11/15.
//  Copyright (c) 2014 HS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HBRPoint : NSObject

@property (assign, nonatomic) float x;
@property (assign, nonatomic) float y;

- (id)initWithX:(float)paramX andY:(float)paramY;

@end
