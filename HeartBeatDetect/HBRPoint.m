//
//  HBRPoint.m
//  af
//
//  Created by Mario Dinata on 11/11/15.
//  Copyright (c) 2014 HS. All rights reserved.
//

#import "HBRPoint.h"

@implementation HBRPoint
@synthesize x;
@synthesize y;

- (id)initWithX:(float)paramX andY:(float)paramY {
    if (self = [super init]) {
        self.x = paramX;
        self.y = paramY;
    }
    
    return self;
}

@end
