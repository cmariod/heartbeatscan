//
//  ScanViewController.m
//  HeartBeatDetect
//
//  Created by Mario Dinata on 11/11/15.
//  Copyright © 2015 Tribal Worldwide SG. All rights reserved.
//

#import "ScanViewController.h"
#import "HBRPoint.h"
#import "RealTimePlot.h"
#import "DDSlackWebhookClient.h"
#import <AudioToolbox/AudioServices.h>

#define measureTime         44.0f
#define averagingRange      52
#define targetFPS           30

@interface ScanViewController () {
    double chartAverageArray[3];
}

- (void)setupFilter;
- (void)turnTorchOn:(bool)on;
- (void)startHBRMeasure;
- (void)stopHBRMeasure;
- (void)updateProgress;
- (void)updateBpm;
- (void)beatDetectionFromRedness:(float)redComponent;
- (void)transferDataToUnity;

- (IBAction)startStopButtonTapped:(id)sender;

@end

@implementation ScanViewController

@synthesize chartView;
@synthesize progressView;
@synthesize startStopButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupFilter];
    
    isStarted = NO;
    
    detailItem = [[RealTimePlot alloc] init];
    [detailItem renderInView:self.chartView withTheme:(id)[NSNull null] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (isStarted) {
        [self stopHBRMeasure];
    }
}

#pragma mark - Privates

- (void)setupFilter {
    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    filter = [[GPUImageAverageColor alloc] init];
    
    [videoCamera addTarget:filter];
    
    HBRData = [[NSMutableArray alloc] init];
    
    [(GPUImageAverageColor *)filter setColorAverageProcessingFinishedBlock:^(CGFloat redComponent, CGFloat greenComponent, CGFloat blueComponent, CGFloat alphaComponent, CMTime frameTime) {
        if (start == nil) {
            start = [NSDate date];
        }
        
        NSTimeInterval totalTimeInSecs = [[NSDate date] timeIntervalSinceDate:start];
        
        if (totalTimeInSecs >= measureTime) {
            bpm = peakCount * 60 / (totalTimeInSecs - 8.0f);
//
            // if within normal human range
            if (bpm >= 30 && bpm <= 210) {
                
                // vibrate to signal user that the test ends
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                
            }
            
            [self performSelectorOnMainThread:@selector(stopHBRMeasure) withObject:nil waitUntilDone:NO];
        }
        
        // beat detection
        [self beatDetectionFromRedness:redComponent];
        
        chartX++;
        
//        if (totalTimeInSecs < measureTime) {
            // update progress
            [self performSelectorOnMainThread:@selector(updateProgress) withObject:nil waitUntilDone:NO];
//        }
    }];
}

- (void)beatDetectionFromRedness:(float)redComponent {
    double redness = redComponent * 255;
    movingAverageArray[chartX % movingAverageArraySize] = redness;
    
    NSTimeInterval totalTimeInSecs = [[NSDate date] timeIntervalSinceDate:start];
    
//    NSLog(@"time:%02f, i:%04lu, red:%f", totalTimeInSecs, (unsigned long)chartX, redness);
    
    chartAverageArray[0] = chartAverageArray[1];
    chartAverageArray[1] = chartAverageArray[2];
    chartAverageArray[2] = redness;
    
    double chartRedness = redness;
    if (chartX > 2) {
        chartRedness = (chartAverageArray[0] + chartAverageArray[1] + chartAverageArray[2]) / 3;
    }
    
    if (chartX > 10) {
        [(RealTimePlot *)detailItem performSelectorOnMainThread:@selector(setData:)
                                                     withObject:@{@"yData": @(chartRedness)}
                                                  waitUntilDone:NO];
    }
    
    float movingAverage = 0;
    NSUInteger movingAverageCount = 0;
    for (NSUInteger i = 0; i < movingAverageArraySize; i++) {
        if (movingAverageArray[i] > 0) {
            movingAverage += movingAverageArray[i];
            movingAverageCount++;
        }
    }
    movingAverage = (movingAverageCount == movingAverageArraySize) ? (movingAverage / movingAverageCount) : 0;
    
//    NSLog(@"time:%f, i:%04lu, red:%f, movingAverage:%f", totalTimeInSecs, (unsigned long)chartX, redness, movingAverage);
    
    if (totalTimeInSecs >= 8.0f) {
        if (redness == 0) { // invalid handler for redness == 0 at any time after 8 secs
            // TODO Error Handler Here
            
            [self stopHBRMeasure];
        }
        
        maxRedness = (redness > maxRedness)? redness : maxRedness;
        if (maxRedness - redness > 40) { // invalid handler for maxredness - redness > 40 after 8 secs, a.k.a. user lift finger from camera
            // TODO Error Handler Here
            
            [self stopHBRMeasure];
        }
        
//        HBRPoint *point;
//        point = [[HBRPoint alloc] initWithX:chartX-1 andY:redness];
        [HBRData addObject:@{@"x": @0, @"y": [NSNumber numberWithFloat:redness / 255.0f], @"z": @0}];
        
//        NSLog(@"Logging redness: %f to HBRData", redness / 255.0f);
    }
    
    // detect bpm
    if (movingAverage > 0) {
        if (isDetectingPeak && movingAverage < redness) {
            // clear movingAverageArray
            movingAverageArray[0] = 0;
            movingAverageArray[1] = 0;
            movingAverageArray[2] = 0;
            movingAverageArray[3] = 0;
            
            isDetectingPeak = NO;
            
            // TODO play sound to detects beep
            
//            NSLog(@"Peak detected");
            
            if (totalTimeInSecs >= 8.0f) {
                peakCount++;
            }
        } else if (!isDetectingPeak && movingAverage > redness) {
//            NSLog(@"Through detected");
            
            // clear movingAverageArray
            movingAverageArray[0] = 0;
            movingAverageArray[1] = 0;
            movingAverageArray[2] = 0;
            movingAverageArray[3] = 0;
            
            isDetectingPeak = YES;
        }
        
        previousMovingAverage = movingAverage;
    }
    
    previousRedness = redness;
    
//    NSLog(@"movingAverageArray %f %f %f %f", movingAverageArray[0], movingAverageArray[1], movingAverageArray[2], movingAverageArray[3]);
//    NSLog(@"redness: %f, movingAvg: %f", redness, movingAverage);
}

- (void)turnTorchOn:(bool)on {
    // check if flashlight available
    if ([videoCamera.inputCamera hasTorch] && [videoCamera.inputCamera hasFlash]) {
        
        [videoCamera.inputCamera lockForConfiguration:nil];
        if (on) {
            [videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOn];
            [videoCamera.inputCamera setFlashMode:AVCaptureFlashModeOn];
        } else {
            // TODO don't turn off if it's iPhone4 / S
            [videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
            [videoCamera.inputCamera setFlashMode:AVCaptureFlashModeOff];
        }
        [videoCamera.inputCamera unlockForConfiguration];
    } else {
        // TODO what to do and test on ipad without flash
    }
}

- (void)startHBRMeasure {
    // clear plot
    [(RealTimePlot *)detailItem generateData];
    
    start = nil;
    
    isDetectingPeak = YES;
    peakCount = 0;
    maxRedness = 0;
    
    chartX = 0;
    timerX = 0;
    bpm = 0;
    
    [HBRData removeAllObjects];
    
    [self turnTorchOn:YES];
    [videoCamera startCameraCapture];
    
    [self.BPMLabel setHidden:YES];
    
    isStarted = YES;
    
    // add new
    [self.startStopButton setTitle:@"Stop" forState:UIControlStateNormal];
}

- (void)stopHBRMeasure {
    // TODO send value to unity
    [self transferDataToUnity];
    
    [videoCamera stopCameraCapture];
    [self turnTorchOn:NO];
    
    [self performSelectorOnMainThread:@selector(updateBpm) withObject:nil waitUntilDone:NO];
    
    isStarted = NO;
    [self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
    start = nil;
}

- (void)updateProgress {
    float progress = [[NSDate date] timeIntervalSinceDate:start] / measureTime;
    if (isStarted) {
        [progressView setProgress:progress animated:YES];
    } else {
        [progressView setProgress:0 animated:YES];
    }
}

- (void)updateBpm {
    self.BPMLabel.text = [NSString stringWithFormat:@"%lu bpm", (unsigned long)bpm];
    if (bpm > 0) {
        [self.BPMLabel setHidden:NO];
    } else {
        [self.BPMLabel setHidden:YES];
    }
}

- (void)transferDataToUnity {
    // preprocess HBR Data for Unity, stretch the value to full range 0 - 1 so will have more movement
    NSArray *HBRDataCopy = [NSArray arrayWithArray:HBRData];
    [HBRData removeAllObjects];
    float min = 1;
    float max = 0;
    float currRedness;
    for (int i=0; i < [HBRDataCopy count]; i++) {
        currRedness = [[[HBRDataCopy objectAtIndex:i] valueForKey:@"y"] floatValue];
        if (currRedness > max) {
            max = currRedness;
        }
        if (currRedness < min) {
            min = currRedness;
        }
    }
    for (int i=0; i < [HBRDataCopy count]; i++) {
        currRedness = [[[HBRDataCopy objectAtIndex:i] valueForKey:@"y"] floatValue];
        
        [HBRData addObject:@{@"x": @0, @"y": [NSNumber numberWithFloat:(currRedness - min) / (max - min)], @"z": @0}];
    }
    
    char fileURIStr[1024];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingString:@"/hbr.json"];
    NSString *fileURI = [NSString stringWithFormat:@"%@", path];
    strcpy (fileURIStr, (char*)[fileURI UTF8String]);
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"text": HBRData} options:0 error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    NSLog(@"HBR JSON String : %@", jsonString);
    
    if ([[NSFileManager defaultManager] createFileAtPath:path contents:jsonData attributes:nil]) {
//        NSLog(@"%@", newData);
    }
    
    // READ FILE
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSLog(@"userdata File Exist %@", path);
//        NSError *err = nil;
//        NSString *content = [NSString stringWithContentsOfFile:path
//                                                      encoding:NSStringEncodingConversionAllowLossy
//                                                         error:&err];
//        NSLog(@"%@", content);
        
//        UnitySendMessage("Plane", "MessageFromIOS", strcpy((char*)malloc(strlen(fileURIStr)+1), fileURIStr));
//        [UnityGetGLView().subviews[0] removeFromSuperview];
        
        [[DDSlackWebhookClient sharedClient] postNotificationToChannel:nil
                                                                  text:jsonString
                                                              username:@"Dr. Koala"
                                                               iconURL:nil
                                                             iconEmoji:@":koala:"
                                                            completion:nil];
    }
    
    
}



#pragma mark - IBActions

- (IBAction)startStopButtonTapped:(id)sender {
    if (isStarted) {
        [self stopHBRMeasure];
    } else {
        [self startHBRMeasure];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
