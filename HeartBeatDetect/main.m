//
//  main.m
//  HeartBeatDetect
//
//  Created by Mario Dinata on 11/11/15.
//  Copyright © 2015 Tribal Worldwide SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
