//
//  RealTimePlot.m
//  CorePlotGallery
//

#import "RealTimePlot.h"

static const double kFrameRate = 5.0;  // frames per second
static const double kAlpha     = 0.25; // smoothing constant

static const NSUInteger kMaxDataPoints = 52;
static NSString *const kPlotIdentifier = @"Data Source Plot";

@interface RealTimePlot()

@property (nonatomic, readwrite, strong) NSMutableArray* plotData;
@property (nonatomic, readwrite, strong) NSMutableArray* originalPlotData;
@property (nonatomic, readwrite, assign) NSUInteger currentIndex;
//@property (nonatomic, readwrite, strong) NSTimer *dataTimer;

@end

@implementation RealTimePlot

@synthesize plotData;
@synthesize originalPlotData;
@synthesize currentIndex;
//@synthesize dataTimer;

+(void)load
{
    [super registerPlotItem:self];
}

-(instancetype)init
{
    if ( (self = [super init]) ) {
        plotData  = [[NSMutableArray alloc] initWithCapacity:kMaxDataPoints];
        originalPlotData  = [[NSMutableArray alloc] initWithCapacity:kMaxDataPoints];
//        dataTimer = nil;

        self.title   = @"";
        self.section = kLinePlots;
    }

    return self;
}

-(void)killGraph
{
//    [self.dataTimer invalidate];
//    self.dataTimer = nil;

    [super killGraph];
}

-(void)generateData
{
    [self.plotData removeAllObjects];
    [self.originalPlotData  removeAllObjects];
    self.currentIndex = 0;
}

-(void)renderInGraphHostingView:(CPTGraphHostingView *)hostingView withTheme:(CPTTheme *)theme animated:(BOOL)animated
{
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
    CGRect bounds = hostingView.bounds;
#else
    CGRect bounds = NSRectToCGRect(hostingView.bounds);
#endif

    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:bounds];
    [self addGraph:graph toHostingView:hostingView];
    [self applyTheme:theme toGraph:graph withDefault:[CPTTheme themeNamed:kCPTDarkGradientTheme]];

//    graph.plotAreaFrame.paddingTop    = self.titleSize * CPTFloat(0.5);
//    graph.plotAreaFrame.paddingRight  = self.titleSize * CPTFloat(0.5);
//    graph.plotAreaFrame.paddingBottom = self.titleSize * CPTFloat(2.625);
//    graph.plotAreaFrame.paddingLeft   = self.titleSize * CPTFloat(2.5);
//    graph.plotAreaFrame.masksToBorder = NO;
    
    graph.plotAreaFrame.paddingTop    = 0;
    graph.plotAreaFrame.paddingRight  = 0;
    graph.plotAreaFrame.paddingBottom = 0;
    graph.plotAreaFrame.paddingLeft   = 0;
    graph.plotAreaFrame.masksToBorder = YES;

    // Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 0.75;
    majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:CPTFloat(0.2)] colorWithAlphaComponent:CPTFloat(0.75)];

    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 0.25;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:CPTFloat(0.1)];
    
    CPTMutableLineStyle *noGridLineStyle = [CPTMutableLineStyle lineStyle];
    noGridLineStyle.lineWidth = 0.0;
    noGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.0];

    // Axes
    // X axis
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x          = axisSet.xAxis;
    x.labelingPolicy        = CPTAxisLabelingPolicyNone;
    x.orthogonalPosition    = @0.0;
    x.majorGridLineStyle    = noGridLineStyle;
    x.minorGridLineStyle    = noGridLineStyle;
    x.minorTicksPerInterval = 9;
    x.labelOffset           = self.titleSize * CPTFloat(0.25);
    x.title                 = @"";
    x.titleOffset           = self.titleSize * CPTFloat(1.5);

    NSNumberFormatter *labelFormatter = [[NSNumberFormatter alloc] init];
    labelFormatter.numberStyle = NSNumberFormatterNoStyle;
    x.labelFormatter           = labelFormatter;

    // Y axis
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy        = CPTAxisLabelingPolicyNone;
    y.orthogonalPosition    = @0.0;
    y.majorGridLineStyle    = noGridLineStyle;
    y.minorGridLineStyle    = noGridLineStyle;
    y.minorTicksPerInterval = 3;
    y.labelOffset           = self.titleSize * CPTFloat(0.25);
    y.title                 = @"";
    y.titleOffset           = self.titleSize * CPTFloat(1.25);
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];

    // Rotate the labels by 45 degrees, just to show it can be done.
    x.labelRotation = CPTFloat(M_PI_4);

    // Create the plot
    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    dataSourceLinePlot.identifier     = kPlotIdentifier;
    dataSourceLinePlot.cachePrecision = CPTPlotCachePrecisionDouble;
    dataSourceLinePlot.interpolation  = CPTScatterPlotInterpolationCurved;

    CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
    lineStyle.lineWidth              = 2.0;
    lineStyle.lineColor              = [CPTColor colorWithComponentRed:128.0f/255.0f green:38.0f/255.0f blue:38.0f/255.0f alpha:1.0f];
    dataSourceLinePlot.dataLineStyle = lineStyle;

    dataSourceLinePlot.dataSource = self;
    [graph addPlot:dataSourceLinePlot];

    // Plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@(kMaxDataPoints - 2)];
//    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@1.0];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:@0.0 length:@4.0];

//    [self.dataTimer invalidate];

//    if ( animated ) {
//        self.dataTimer = [NSTimer timerWithTimeInterval:1.0 / kFrameRate
//                                                 target:self
//                                               selector:@selector(newData:)
//                                               userInfo:nil
//                                                repeats:YES];
//        [[NSRunLoop mainRunLoop] addTimer:self.dataTimer forMode:NSRunLoopCommonModes];
//    }
//    else {
//        self.dataTimer = nil;
//    }
}

-(void)dealloc
{
//    [dataTimer invalidate];
}

#pragma mark -
#pragma mark Timer callback

-(void)newData:(NSTimer *)theTimer
{
    CPTGraph *theGraph = (self.graphs)[0];
    CPTPlot *thePlot   = [theGraph plotWithIdentifier:kPlotIdentifier];

    if ( thePlot ) {
        if ( self.plotData.count >= kMaxDataPoints ) {
            [self.plotData removeObjectAtIndex:0];
            [thePlot deleteDataInIndexRange:NSMakeRange(0, 1)];
        }

        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
        NSUInteger location       = (self.currentIndex >= kMaxDataPoints ? self.currentIndex - kMaxDataPoints + 2 : 0);

        CPTPlotRange *oldRange = [CPTPlotRange plotRangeWithLocation:@( (location > 0) ? (location - 1) : 0 )
                                                              length:@(kMaxDataPoints - 2)];
        CPTPlotRange *newRange = [CPTPlotRange plotRangeWithLocation:@(location)
                                                              length:@(kMaxDataPoints - 2)];

        [CPTAnimation animate:plotSpace
                     property:@"xRange"
                fromPlotRange:oldRange
                  toPlotRange:newRange
                     duration:CPTFloat(1.0 / kFrameRate)];

        self.currentIndex++;
        [self.plotData addObject:@( (1.0 - kAlpha) * [[self.plotData lastObject] doubleValue] + kAlpha * arc4random() / (double)UINT32_MAX )];
        [thePlot insertDataAtIndex:self.plotData.count - 1 numberOfRecords:1];
    }
}

// new method to do on the fly calculation of min max, and make the plot range re-adjust itself, by feeding this method with real value
-(void)setData:(NSDictionary *)data
{
    CPTGraph *theGraph = [self.graphs objectAtIndex:0];
    CPTPlot *thePlot   = [theGraph plotWithIdentifier:kPlotIdentifier];
    
    if ( thePlot ) {
        if ( originalPlotData.count >= kMaxDataPoints ) {
            [originalPlotData removeObjectAtIndex:0];
            [thePlot deleteDataInIndexRange:NSMakeRange(0, 1)];
        }
        
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)theGraph.defaultPlotSpace;
        NSUInteger location       = (currentIndex >= kMaxDataPoints ? currentIndex - kMaxDataPoints + 2 : 0);
        
        CPTPlotRange *newXRange = [CPTPlotRange plotRangeWithLocation:[NSNumber numberWithInteger:location]
                                                               length:[NSNumber numberWithInt:(kMaxDataPoints - 2)]];
        
        // view controller feed yrange
//        CPTPlotRange *newYRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromUnsignedInteger(0)
//                                                               length:CPTDecimalFromFloat([[data objectForKey:@"range"] floatValue])];
        
        // dynamicly calculate yrange and all previous value based on minimum value within range
        double chartMax = [[data objectForKey:@"yData"] floatValue], chartMin = [[data objectForKey:@"yData"] floatValue], currentData;
        for (int i = 0; i < [originalPlotData count]; i++) {
            currentData = [[originalPlotData objectAtIndex:i] floatValue];
            if (currentData > chartMax) chartMax = currentData;
            if (currentData < chartMin) chartMin = currentData;
        }
        [plotData removeAllObjects];
        for (int i = 0; i < [originalPlotData count]; i++) {
            [plotData addObject:@([[originalPlotData objectAtIndex:i] floatValue] - chartMin + ((chartMax - chartMin) * 0.1f))];
        }
        
//        NSLog(@"i:%04d, red:%f, max:%f, min:%f, range:%f", currentIndex, [[data objectForKey:@"yData"] floatValue], chartMax, chartMin, chartMax-chartMin);
        
        CGFloat rangeY = (chartMax - chartMin) * 1.2f;
        rangeY = (isnan(rangeY))? 0 : rangeY;
        CPTPlotRange *newYRange = [CPTPlotRange plotRangeWithLocation:[NSNumber numberWithInt:0]
                                                               length:[NSNumber numberWithFloat:rangeY]];
        
        [CPTAnimation animate:plotSpace
                     property:@"yRange"
                fromPlotRange:plotSpace.yRange
                  toPlotRange:newYRange
                     duration:CPTFloat(1.0 / kFrameRate)];
        
        [CPTAnimation animate:plotSpace
                     property:@"xRange"
                fromPlotRange:plotSpace.xRange
                  toPlotRange:newXRange
                     duration:CPTFloat(1.0 / kFrameRate)];
        
        currentIndex++;
        [originalPlotData addObject:[data objectForKey:@"yData"]]; // add the data to originalData
//        [thePlot insertDataAtIndex:originalPlotData.count - 1 numberOfRecords:1];
        [thePlot reloadData]; // relaod all data, as there might be change to all data, if there's a new chartMin value
    }
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return self.plotData.count;
}

-(id)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    NSNumber *num = nil;

    switch ( fieldEnum ) {
        case CPTScatterPlotFieldX:
            num = @(index + self.currentIndex - self.plotData.count);
            break;

        case CPTScatterPlotFieldY:
            num = self.plotData[index];
            break;

        default:
            break;
    }

    return num;
}

@end
